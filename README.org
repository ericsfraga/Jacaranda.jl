# Jacaranda.jl

Interface to the Jacaranda system written in Java.

Jacaranda is an extensible objective oriented framework for process design and optimization.  Further information on Jacaranda can be found [[https://www.ucl.ac.uk/~ucecesf/research.html][in the author's personal web pages]] at [[https://www.ucl.ac.uk/][UCL]] (University College London).

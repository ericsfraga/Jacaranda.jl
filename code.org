#+title: The Julia-Jacaranda interface
#+author: Professor Eric S Fraga

* The Jacaranda module
:PROPERTIES:
:header-args:julia: :tangle "src/Jacaranda.jl" :comments link
:END:
** module start
#+name: modulestart
#+begin_src julia
  # All code copyright © Eric S Fraga. 
  # Date of last change in version variable below.
  module Jacaranda

  # all access to Jacaranda objects is through this module which
  # provides a Julia interface to the Java Native Interface (jni)
  using JavaCall

  version = "[2022-01-12 15:59]"
  function __init__()
      # println("# -*- mode: org; eval: (org-content 3); -*-")
      # println(": Jacaranda Julia interface, last change $version")
  end
#+end_src
** data types (structures)
*** MI: mixed integer representation
#+name: mi
#+begin_src julia
  struct MI
      x :: Vector{Float64}
      y :: Vector{Int32}
  end
#+end_src
** extracting Jacaranda variables
A flowsheet object has a number of variable objects including the last point evaluated, =getVariables=, the lower bound on the design variables, =getLowerBounds=, and the upper bounds on those variables, =getUpperBounds=.  The method to use is passed as an argument to this method.  The result of this method is one of 3 possibilities:
1. a vector of real numbers
2. a vector of integer numbers
3. an =MI= data structure consisting of both real and integer vectors
#+name: variables
#+begin_src julia
  function variables(flowsheet, method :: String)
      juov = @jimport jacaranda.util.opt.Variables
      v = jcall(flowsheet, method, juov, ())
      x = jcall(v, "getX", Array{jdouble,1}, ())
      nx = length(x)
      y = jcall(v, "getY", Array{jint,1}, ())
      ny = length(y)
      if nx > 0
          if ny > 0
              ret = MI(x, y)
          else
              ret = x
          end
      else
          ret = y
      end
      ret
  end
#+end_src

** initialisation
#+name: init
#+begin_src julia
  function init(inputfile :: String, # the Jacaranda input file
                objectivefunction :: String; # the name of the Jacaranda object
                classpath = "/home/ucecesf/synced/research/jacaranda/src/java")
      # println("* Jacaranda initialization")
      # set the class path to where Jacaranda is found on this system
      JavaCall.init([string("-Djava.class.path=", classpath)])
      # create an Input object with the input file given
      jui = @jimport jacaranda.ui.Input
      input = jui((JString, ), inputfile)
      # use the parser to retrieve the Jacaranda object corresponding to
      # the objective function specified.  The assumption here is that
      # this object will be a Jacaranda Flowsheet object.
      jup = @jimport jacaranda.ui.Parser
      parser = jcall(input, "getParser", jup, ())
      jdpf = @jimport jacaranda.design.ps.Flowsheet
      object = jcall(parser, "getIdentifier", JObject, (JString,), objectivefunction)
      flowsheet = JavaCall.convert(jdpf, object)
      # determine the number of objective function values.  Note that
      # this will often include objective functions that actually
      # represent constraint violations.  The evaluation of the
      # objective function will need to separate out the actual
      # objective function values from the constraint violations.
      nf = jcall(flowsheet, "getNF", jint, ())
      # now get the lower and upper bounds as well as the current value
      # of all design variables which will be used as an initial guess
      # for any optimization method.  the design variables may include
      # real and/or integer values.  If both are present, a
      # mixed-integer data structure will be created; otherwise, the
      # initial guess and the lower and upper bounds will be represented
      # by vectors of real or integer values.
      x0 = variables(flowsheet, "getVariables")
      lower = variables(flowsheet, "getLowerBounds")
      upper = variables(flowsheet, "getUpperBounds")
      (flowsheet, x0, lower, upper, nf)
  end
#+end_src
** evaluate
Given a flowsheet object, evaluate a given design point and return the full set of objective function values.  These objective function values may often consist of the real objective functions and the constraint violations.  The caller needs to know how to distinguish between these.

The evaluation interface implemented currently is only for problems where all the design variables are real numbers.
#+name: evaluate
#+begin_src julia
  function evaluate(flowsheet, v :: Vector{Float64}) :: Vector{Float64}
      jcall(flowsheet, "evaluate", Array{jdouble,1}, (Array{jdouble,1},), v)
  end
#+end_src
** print
#+begin_src julia
  function print(object)
      jcall(object, "print", Nothing, ())
  end
#+end_src
** end of module
#+begin_src julia
  end                             # module
#+end_src
* Tests
** init
:PROPERTIES:
:header-args:julia: :tangle "test/init.jl" :comments yes
:END:
#+name: testinit
#+begin_src julia
  using Jacaranda
  (flowsheet, x0, lower, upper, nf) = Jacaranda.init("clben-process-flowsheet.in", "clben")
  @show flowsheet
  @show x0
  @show lower
  @show upper
  @show nf
#+end_src
** evaluate
:PROPERTIES:
:header-args:julia: :tangle "test/evaluate.jl" :comments yes
:END:
#+name: testevaluate
#+begin_src julia
  using Jacaranda
  (flowsheet, x0, lower, upper, nf) = Jacaranda.init("clben-process-flowsheet.in", "clben")
  @show flowsheet
  @show x0
  @show lower
  @show upper
  @show nf
  z = Jacaranda.evaluate(flowsheet, x0)
  @show z
  @show Jacaranda.evaluate(flowsheet, lower)
  @show Jacaranda.evaluate(flowsheet, upper)
  @show Jacaranda.evaluate(flowsheet, lower+(upper-lower)/2.0)
  Jacaranda.print(flowsheet)
#+end_src

** optimise
:PROPERTIES:
:header-args:julia: :tangle "test/optimise.jl" :comments yes
:END:
Test out the interface by using Fresa to optimise the process.
#+name: testoptimise
#+begin_src julia
  using Jacaranda
  using Fresa
  (flowsheet, x0, lower, upper, nf) = Jacaranda.init("clben-process-flowsheet.in", "clben")
  function f(x :: Vector{Float64}, flowsheet)
      z = Jacaranda.evaluate(flowsheet, x)
      (z[1:2], maximum(z[3:5]))
  end
  p0 = [Fresa.createpoint(x0, f, flowsheet)]
  domain = Fresa.Domain(x -> lower, x -> upper)
  pareto, population = Fresa.solve(f, p0, domain, parameters=flowsheet, ngen = 1000, npop = 100)
  println("**** Pareto front:")
  println("#+plot: ind:1 deps:(2) with:points")
  println(population[pareto])
  println("**** Full population")
  println(population)
#+end_src

* Recent change history
Summary of the most recent changes to the package:
#+name: changehistoryshellblock
#+begin_src shell :exports results :results output
  git log --date=format:"%Y-%m-%d %H:%M" --pretty=format:"%ad %s" -20 code.org
#+end_src
A full history is [[https://gitlab.com/ericsfraga/Jacaranda.jl][at the gitlab site]].
* settings                                                    :noexport:
** org
#+PROPERTY: header-args :cache yes
*** COMMENT beamer settings
#+startup: beamer
Change this setting depending on whether there are sections for the talk or not, with 2 for sections, 1 for no sections and 3 for subsections as well.
#+options: H:1
The theme can be ~minimal~, ~progressbar~, or anything else.
#+beamer_theme: minimal
If links are used directly, colour them gray.
#+latex_header: \hypersetup{colorlinks=true,urlcolor=gray}
#+macro: actualdate 5 April 2018
#+macro: where MCDA87, Delft
#+institute: University College London
#+latex_header_extra: \institute{University College London (UCL)}
*** date formatting with version information           :ignoreheading:
**** COMMENT src
#+NAME: mydateline
#+BEGIN_SRC emacs-lisp
(format "#+DATE: \\copyright{} %s\n" *this*) 
#+END_SRC

src_shell[:post mydateline() :results raw]{echo -n $(date +%Y) '@@latex:\\ \vspace*{0.1cm} \tiny \color{gray}@@' v$(src list -1 -f '{1} ({3})')}
**** git
#+NAME: mydateline
#+BEGIN_SRC emacs-lisp
(format "#+DATE: \\copyright{} %s\n" *this*) 
#+END_SRC

src_shell[:post mydateline() :results raw]{echo -n $(date +%Y) '@@latex:\\ \vspace*{0.1cm} \tiny \color{gray}@@' version $(git log --format=format:"%ad %h" --date=short | head -1 )} 
**** COMMENT mercurial
#+NAME: mydateline
#+BEGIN_SRC emacs-lisp
(format "#+DATE: \\copyright{} %s\n" *this*) 
#+END_SRC

src_shell[:post mydateline() :results raw]{echo -n $(date +%Y) '@@latex:\\ \vspace*{0.1cm} \tiny \color{gray}@@' version $(hg log slides.org | head -1 | sed -e 's/^.* \([0-9]*\):.*$/\1/')} 
**** COMMENT rcs
#+latex_header: \usepackage{rcs}
#+latex_header: \RCS $Revision: 1.31 $
#+latex_header: \RCS $Date: 2020/10/06 11:14:27 $
#+date: @@latex:\ifdef{\institute}{@@ {{{actualdate}}} @@latex:\\@@ {{{where}}} @@latex:\\ \vfill\hfill{\tiny\color{gray}v\RCSRevision~\RCSDate}}{@@ @@latex:}@@

*** julia
Julia definition (c) 2014 Jubobs, from https://tex.stackexchange.com/questions/212793/how-can-i-typeset-julia-code-with-the-listings-package
#+latex_header: \lstdefinelanguage{Julia}%
#+latex_header:  {morekeywords={abstract,Array,break,case,catch,const,continue,do,else,elseif,%
#+latex_header:      end,export,false,Float64,for,function,immutable,import,importall,if,in,%
#+latex_header:      macro,module,mutable,new,otherwise,quote,print,return,struct,switch,true,try,type,typealias,%
#+latex_header:      using,while},%
#+latex_header:   sensitive=true,%
#+latex_header:   alsoother={$},%
# +latex_header:   keywordstyle=\color{green},%
#+latex_header:   morecomment=[l]\#,%
#+latex_header:   morecomment=[n]{\#=}{=\#},%
#+latex_header:   morestring=[s]{"}{"},%
#+latex_header:   morestring=[m]{'}{'},%
#+latex_header:   stringstyle=\color{gray},%
#+latex_header: }[keywords,comments,strings]%
*** latex
**** for solution formatting
#+latex_header: \usepackage[backgroundcolor=yellow!10!white]{mdframed}
#+latex_header: \tikzstyle{unit}=[rectangle, draw=blue!80!black, fill=black!5!white]
#+latex_header: \tikzstyle{stream}=[rectangle, draw=white, fill=yellow!50!white]
**** listings
Define special characters that may appear in code, especially Julia code which allows all Unicode characters.  I often use \in and \pm along with greek letters:
#+latex_header: \lstset{literate=
#+latex_header: {±}{{$\pm$}}1
#+latex_header: {∈}{{$\in$}}1
#+latex_header: }
**** white box for presentations on black background
#+latex_header: \definecolor{verypaleblue}{rgb}{0.95,0.95,1}
#+latex_header: \makeatletter\newenvironment{whitebox}{\begin{lrbox}{\@tempboxa}\begin{minipage}{0.98\columnwidth}\centering}{\end{minipage}\end{lrbox}\colorbox{verypaleblue}{\usebox{\@tempboxa}}}\makeatother
*** maxima
#+latex_header: \lstdefinelanguage{Maxima}%
#+latex_header:  {morekeywords={define,diff,print,solve},%
#+latex_header:   sensitive=true,%
#+latex_header:   alsoother={$},%
#+latex_header:   morecomment=[l]\#,%
#+latex_header:   morecomment=[n]{\#=}{=\#},%
#+latex_header:   morestring=[s]{"}{"},%
#+latex_header:   morestring=[m]{'}{'},%
#+latex_header: }[keywords,comments,strings]%
*** macros
**** calc: for embedded Emacs calc use
#+macro: calc @@latex:{\color{green!50!black}\texttt{ $1 }}@@
**** COMMENT calc: short better formatted version of calculate macro
If the second argument is not given, no variable is stored or shown in the output.

#+macro: calc src_emacs-lisp[:results latex]{(esf/calc-and-output "$1" "$2")}

The macro relies on the following code:

#+name: calc-and-output
#+begin_src emacs-lisp :results silent :exports none
  (defun esf/calc-and-output (expression variable)
    (let ((result (string-to-number (calc-eval (format "evalv(%s)" expression)))))
      (message "Expression %s results in %s" expression result)
      (if (string= "" variable)
          (format "%s = \\fbox{%s}" expression result)
        (progn
          (eval (format "(setq var-%s %s)" variable result))
          (format "\\texttt{%s} \\(\\gets\\) %s = \\fbox{%s}" variable expression result))
        )
      ))
#+end_src 

**** COMMENT calculate: use emacs calc to evaluate expressions and assign variables
# use listings to export the code evaluated
#+latex_header: \lstdefinelanguage{calc}{}
# evaluate the code and format the output
#+macro: calculate $2 \(\gets\) src_calc[:exports code]{$1} = @@latex:\fbox{@@ src_emacs-lisp{(setq var-$2 (string-to-number (calc-eval "evalv($1)")))} @@latex:}@@
**** cite: macro for citing work and url to actual source
# +macro: cite @@latex:\vfill\Citation{$1}@@@@html:<p style="text-align: right; color: gray;">@@[[$2][$1]]@@html:</p>@@
# alternative cite macro for LaTeX only but with working link
#+macro: cite [[$2][@@latex:\vfill\Citation{$1}@@]]
#+latex_header: \newcommand{\Citation}[1]{\hfill{\scriptsize{\color{gray}#1}}}

**** overlay: for absolute positioning of images etc.
#+latex_header: \usepackage[overlay]{textpos} \TPGrid[0pt,0pt]{20}{20}
#+macro: overlay @@latex:\begin{textblock}{$4}($2,$3)@@[[file:$1]]@@latex:\end{textblock}@@

*** org startup on file visit
#+name: startup
#+begin_src emacs-lisp :results none
  (org-content 2)
#+end_src

** local variables
# Local Variables:
# org-confirm-babel-evaluate: nil
# eval: (org-sbe "startup")
# time-stamp-line-limit: 1000
# time-stamp-format: "[%Y-%m-%d %H:%M]"
# time-stamp-active: t
# time-stamp-start: "version = \""
# time-stamp-end: "\""
# End:

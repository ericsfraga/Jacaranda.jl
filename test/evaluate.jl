# [[file:../code.org::testevaluate][testevaluate]]
; using Jacaranda
(flowsheet, x0, lower, upper, nf) = Jacaranda.init("clben-process-flowsheet.in", "clben")
@show flowsheet
@show x0
@show lower
@show upper
@show nf
z = Jacaranda.evaluate(flowsheet, x0)
@show z
@show Jacaranda.evaluate(flowsheet, lower)
@show Jacaranda.evaluate(flowsheet, upper)
@show Jacaranda.evaluate(flowsheet, lower+(upper-lower)/2.0)
Jacaranda.print(flowsheet);
# testevaluate ends here

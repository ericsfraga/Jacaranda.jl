# [[file:../code.org::testoptimise][testoptimise]]
; using Jacaranda
using Fresa
(flowsheet, x0, lower, upper, nf) = Jacaranda.init("clben-process-flowsheet.in", "clben")
function f(x :: Vector{Float64}, flowsheet)
    z = Jacaranda.evaluate(flowsheet, x)
    (z[1:2], maximum(z[3:5]))
end
p0 = [Fresa.createpoint(x0, f, flowsheet)]
domain = Fresa.Domain(x -> lower, x -> upper)
pareto, population = Fresa.solve(f, p0, domain, parameters=flowsheet, ngen = 1000, npop = 100)
println("**** Pareto front:")
println("#+plot: ind:1 deps:(2) with:points")
println(population[pareto])
println("**** Full population")
println(population);
# testoptimise ends here

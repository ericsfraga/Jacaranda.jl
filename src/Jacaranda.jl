# [[file:../code.org::modulestart][modulestart]]
; # All code copyright © Eric S Fraga. 
# Date of last change in version variable below.
module Jacaranda

# all access to Jacaranda objects is through this module which
# provides a Julia interface to the Java Native Interface (jni)
using JavaCall

version = "[2022-01-12 15:59]"
function __init__()
    # println("# -*- mode: org; eval: (org-content 3); -*-")
    # println(": Jacaranda Julia interface, last change $version")
end;
# modulestart ends here

# [[file:../code.org::mi][mi]]
; struct MI
    x :: Vector{Float64}
    y :: Vector{Int32}
end;
# mi ends here

# [[file:../code.org::variables][variables]]
; function variables(flowsheet, method :: String)
    juov = @jimport jacaranda.util.opt.Variables
    v = jcall(flowsheet, method, juov, ())
    x = jcall(v, "getX", Array{jdouble,1}, ())
    nx = length(x)
    y = jcall(v, "getY", Array{jint,1}, ())
    ny = length(y)
    if nx > 0
        if ny > 0
            ret = MI(x, y)
        else
            ret = x
        end
    else
        ret = y
    end
    ret
end;
# variables ends here

# [[file:../code.org::init][init]]
; function init(inputfile :: String, # the Jacaranda input file
              objectivefunction :: String; # the name of the Jacaranda object
              classpath = "/home/ucecesf/synced/research/jacaranda/src/java")
    # println("* Jacaranda initialization")
    # set the class path to where Jacaranda is found on this system
    JavaCall.init([string("-Djava.class.path=", classpath)])
    # create an Input object with the input file given
    jui = @jimport jacaranda.ui.Input
    input = jui((JString, ), inputfile)
    # use the parser to retrieve the Jacaranda object corresponding to
    # the objective function specified.  The assumption here is that
    # this object will be a Jacaranda Flowsheet object.
    jup = @jimport jacaranda.ui.Parser
    parser = jcall(input, "getParser", jup, ())
    jdpf = @jimport jacaranda.design.ps.Flowsheet
    object = jcall(parser, "getIdentifier", JObject, (JString,), objectivefunction)
    flowsheet = JavaCall.convert(jdpf, object)
    # determine the number of objective function values.  Note that
    # this will often include objective functions that actually
    # represent constraint violations.  The evaluation of the
    # objective function will need to separate out the actual
    # objective function values from the constraint violations.
    nf = jcall(flowsheet, "getNF", jint, ())
    # now get the lower and upper bounds as well as the current value
    # of all design variables which will be used as an initial guess
    # for any optimization method.  the design variables may include
    # real and/or integer values.  If both are present, a
    # mixed-integer data structure will be created; otherwise, the
    # initial guess and the lower and upper bounds will be represented
    # by vectors of real or integer values.
    x0 = variables(flowsheet, "getVariables")
    lower = variables(flowsheet, "getLowerBounds")
    upper = variables(flowsheet, "getUpperBounds")
    (flowsheet, x0, lower, upper, nf)
end;
# init ends here

# [[file:../code.org::evaluate][evaluate]]
; function evaluate(flowsheet, v :: Vector{Float64}) :: Vector{Float64}
    jcall(flowsheet, "evaluate", Array{jdouble,1}, (Array{jdouble,1},), v)
end;
# evaluate ends here

# [[file:../code.org::*print][print:1]]
; function print(object)
    jcall(object, "print", Nothing, ())
end;
# print:1 ends here

# [[file:../code.org::*end of module][end of module:1]]
; end                             # module;
# end of module:1 ends here
